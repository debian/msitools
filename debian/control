Source: msitools
Maintainer: Stephen Kitt <skitt@debian.org>
Section: utils
Priority: optional
Build-Depends: bats,
               bison,
               debhelper-compat (= 13),
               gobject-introspection,
               libgcab-dev,
               libgirepository1.0-dev,
               libglib2.0-dev,
               libgsf-1-dev,
               libxml2-dev,
               meson (>= 0.52),
               valac
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian/msitools
Vcs-Git: https://salsa.debian.org/debian/msitools.git
Homepage: https://wiki.gnome.org/msitools
Rules-Requires-Root: no

Package: msitools
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Windows Installer file manipulation tool
 msitools contains a number of programs to create, inspect and extract
 Windows Installer (.msi) files.
 .
 The following tools are included:
  - msiinfo: inspects MSI packages
  - msibuild: builds MSI packages
  - msidiff: compares two MSI packages' contents
  - msidump: dumps raw MSI tables and stream contents
  - msiextract: extracts files contained in MSI packages

Package: wixl
Architecture: any
Multi-Arch: foreign
Depends: wixl-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Windows Installer creation tool
 wixl builds Windows Installer (.msi) packages from XML documents, in
 a similar fashion to the WiX toolset.
 .
 This package includes wixl-heat, which builds WiX XML fragments from
 lists of files and directories.

Package: wixl-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Windows Installer creation tool — data files
 wixl builds Windows Installer (.msi) packages from XML documents, in
 a similar fashion to the WiX toolset.
 .
 This package contains the package descriptions.

Package: libmsi-1.0-0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Windows Installer file manipulation library
 libmsi provides functions to manipulate Windows Installer (.msi)
 files, both for reading and writing their contents and querying their
 databases.
 .
 This package contains the runtime library.

Package: libmsi-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: gir1.2-libmsi-1.0 (= ${binary:Version}),
         libmsi-1.0-0 (= ${binary:Version}),
         ${misc:Depends}
Description: Windows Installer file manipulation library - development files
 libmsi provides functions to manipulate Windows Installer (.msi)
 files, both for reading and writing their contents and querying their
 databases.
 .
 This package contains the header files and libraries required to
 develop software using libmsi.

Package: gir1.2-libmsi-1.0
Architecture: any
Multi-Arch: same
Section: introspection
Depends: ${gir:Depends},
         ${misc:Depends},
         libmsi-1.0-0
Description: Windows Installer file manipulation library - gir bindings
 libmsi provides functions to manipulate Windows Installer (.msi)
 files, both for reading and writing their contents and querying their
 databases.
 .
 This package contains the GObject introspection repository bindings.
